#include "BQ27421.hpp"

#include <wiringPi.h>
#include <cstdio>
#include <unistd.h>

#define GPOUT_PIN               25
#define BATTERY_CAPACITY        3350 * 2
#define PERCENTAGE_INTERVAL     1

static BQ27421 bat;

static int i = 0;
void interrupt_handler() {
    printf("INTERRUPT HAPPENED!!!!!! %d\n", ++i);
}

int main(int argc, char const *argv[])
{

    // GPIO
    wiringPiSetupGpio();
    wiringPiISR(GPOUT_PIN, INT_EDGE_RISING, &interrupt_handler);

    // BQ27421
    // BQ27421 bat = BQ27421();
    if (!bat.begin()) {
        return -1;
    }

    bat.enterConfig(); // To configure the values below, you must be in config mode
    bat.setCapacity(BATTERY_CAPACITY); // Set the battery capacity
    bat.setGPOUTPolarity(HIGH); // Set GPOUT to active-high
    bat.setGPOUTFunction(SOC_INT); // Set GPOUT to SOC_INT mode
    bat.setSOCIDelta(PERCENTAGE_INTERVAL); // Set percentage change integer
    bat.exitConfig(); // Exit config mode to save changes

    unsigned int old_volt = -1;

    while (1) {
        sleep(1);
        if (bat.voltage() != old_volt){
            old_volt = bat.voltage();
            printf("voltage:\t %u mV\n", old_volt);
            printf("capaticy:\t %u/%u mAh\n", bat.capacity(REMAIN), bat.capacity(FULL));
            printf("soc:\t\t %u%%\n", bat.soc());
        }
        // printf("current:\t %d mA\n", bat.current());
        // printf("temperature:\t %.2f °C\n", bat.temperature() * 0.1 - 273.15);
        // printf("power:\t\t %d\n", bat.power());

        // printf("soh:\t\t %u%%\n", bat.soh());
    }


    // printf("socFlag:\t\t %s\n", bat.socFlag() ? "T" : "F");
    // printf("socfFlag:\t\t %s\n", bat.socfFlag() ? "T" : "F");

    // printf("SOC1 Threshold:\t %u\n", bat.SOC1SetThreshold());
    // printf("SOCF Threshold:\t %u\n", bat.SOCFSetThreshold());

    // bat.setSOC1Thresholds(30, 35);
    // bat.setSOCFThresholds(20, 25);

    // printf("SOC1 Threshold:\t %u\n", bat.SOC1SetThreshold());
    // printf("SOCF Threshold:\t %u\n", bat.SOCFSetThreshold());

    // printf("socFlag:\t\t %s\n", bat.socFlag() ? "T" : "F");
    // printf("socfFlag:\t\t %s\n", bat.socfFlag() ? "T" : "F");

    // bat.SOC1ClearThreshold();
    // bat.SOCFClearThreshold();

    return 0;
}