#include "BQ27421.hpp"
#include "BQ27421_Definitions.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <unistd.h>
#include <cstring>      // memcpy

// template <typename T>
inline uint8_t constrain(uint8_t x, uint8_t a, uint8_t b)
{
    return x < a ? a : (x > b ? b : x);
}

inline void delay(unsigned long miliseconds) {
    usleep(miliseconds * 1000);
}

/*****************************************************************************
 ************************** Initialization Functions *************************
 *****************************************************************************/
// Initializes class variables
BQ27421::BQ27421() :
    _deviceAddress(BQ27421_I2C_ADDRESS),
    _device_fd(-1),
    _sealFlag(false),
    _userConfigControl(false)
{
}

// Initializes I2C and verifies communication with the BQ27421.
bool BQ27421::begin(void)
{
    uint16_t deviceID = 0;

    i2c_init(I2C_PATH); // Initialize I2C master

    deviceID = deviceType(); // Read deviceType from BQ27421

    if (deviceID == BQ27421_DEVICE_ID)
    {
        return true; // If device ID is valid, return true
    }

    return false; // Otherwise return false
}

// Configures the design capacity of the connected battery.
bool BQ27421::setCapacity(uint16_t capacity)
{
    // Write to STATE subclass (82) of BQ27421 extended memory.
    // Offset 0x0A (10)
    // Design capacity is a 2-byte piece of data - MSB first
    uint8_t capMSB = capacity >> 8;
    uint8_t capLSB = capacity & 0x00FF;
    uint8_t capacityData[2] = {capMSB, capLSB};
    return writeExtendedData(BQ27421_ID_STATE, 10, capacityData, 2);
}

/*****************************************************************************
 ********************** Battery Characteristics Functions ********************
 *****************************************************************************/

// Reads and returns the battery voltage
uint16_t BQ27421::voltage(void)
{
    return readWord(BQ27421_COMMAND_VOLTAGE);
}

// Reads and returns the specified current measurement
int16_t BQ27421::current(current_measure type)
{
    int16_t current = 0;
    switch (type)
    {
    case AVG:
        current = (int16_t) readWord(BQ27421_COMMAND_AVG_CURRENT);
        break;
    case STBY:
        current = (int16_t) readWord(BQ27421_COMMAND_STDBY_CURRENT);
        break;
    case MAX:
        current = (int16_t) readWord(BQ27421_COMMAND_MAX_CURRENT);
        break;
    }

    return current;
}

// Reads and returns the specified capacity measurement
uint16_t BQ27421::capacity(capacity_measure type)
{
    uint16_t capacity = 0;
    switch (type)
    {
    case REMAIN:
        return readWord(BQ27421_COMMAND_REM_CAPACITY);
        break;
    case FULL:
        return readWord(BQ27421_COMMAND_FULL_CAPACITY);
        break;
    case AVAIL:
        capacity = readWord(BQ27421_COMMAND_NOM_CAPACITY);
        break;
    case AVAIL_FULL:
        capacity = readWord(BQ27421_COMMAND_AVAIL_CAPACITY);
        break;
    case REMAIN_F:
        capacity = readWord(BQ27421_COMMAND_REM_CAP_FIL);
        break;
    case REMAIN_UF:
        capacity = readWord(BQ27421_COMMAND_REM_CAP_UNFL);
        break;
    case FULL_F:
        capacity = readWord(BQ27421_COMMAND_FULL_CAP_FIL);
        break;
    case FULL_UF:
        capacity = readWord(BQ27421_COMMAND_FULL_CAP_UNFL);
        break;
    case DESIGN:
        capacity = readWord(BQ27421_EXTENDED_CAPACITY);
    }

    return capacity;
}

// Reads and returns measured average power
int16_t BQ27421::power(void)
{
    return (int16_t) readWord(BQ27421_COMMAND_AVG_POWER);
}

// Reads and returns specified state of charge measurement
uint16_t BQ27421::soc(soc_measure type)
{
    uint16_t socRet = 0;
    switch (type)
    {
    case FILTERED:
        socRet = readWord(BQ27421_COMMAND_SOC);
        break;
    case UNFILTERED:
        socRet = readWord(BQ27421_COMMAND_SOC_UNFL);
        break;
    }

    return socRet;
}

// Reads and returns specified state of health measurement
uint8_t BQ27421::soh(soh_measure type)
{
    uint16_t sohRaw = readWord(BQ27421_COMMAND_SOH);
    uint8_t sohStatus = sohRaw >> 8;
    uint8_t sohPercent = sohRaw & 0x00FF;

    if (type == PERCENT)
        return sohPercent;
    else
        return sohStatus;
}

// Reads and returns specified temperature measurement
uint16_t BQ27421::temperature(temp_measure type)
{
    uint16_t temp = 0;
    switch (type)
    {
    case BATTERY:
        temp = readWord(BQ27421_COMMAND_TEMP);
        break;
    case INTERNAL_TEMP:
        temp = readWord(BQ27421_COMMAND_INT_TEMP);
        break;
    }
    return temp;
}

/*****************************************************************************
 ************************** GPOUT Control Functions **************************
 *****************************************************************************/
// Get GPOUT polarity setting (active-high or active-low)
bool BQ27421::GPOUTPolarity(void)
{
    uint16_t opConfigRegister = opConfig();

    return (opConfigRegister & BQ27421_OPCONFIG_GPIOPOL);
}

// Set GPOUT polarity to active-high or active-low
bool BQ27421::setGPOUTPolarity(bool activeHigh)
{
    uint16_t oldOpConfig = opConfig();

    // Check to see if we need to update opConfig:
    if ((activeHigh && (oldOpConfig & BQ27421_OPCONFIG_GPIOPOL)) ||
            (!activeHigh && !(oldOpConfig & BQ27421_OPCONFIG_GPIOPOL)))
        return true;

    uint16_t newOpConfig = oldOpConfig;
    if (activeHigh)
        newOpConfig |= BQ27421_OPCONFIG_GPIOPOL;
    else
        newOpConfig &= ~(BQ27421_OPCONFIG_GPIOPOL);

    return writeOpConfig(newOpConfig);
}

// Get GPOUT function (BAT_LOW or SOC_INT)
bool BQ27421::GPOUTFunction(void)
{
    uint16_t opConfigRegister = opConfig();

    return (opConfigRegister & BQ27421_OPCONFIG_BATLOWEN);
}

// Set GPOUT function to BAT_LOW or SOC_INT
bool BQ27421::setGPOUTFunction(gpout_function function)
{
    uint16_t oldOpConfig = opConfig();

    // Check to see if we need to update opConfig:
    if ((function && (oldOpConfig & BQ27421_OPCONFIG_BATLOWEN)) ||
            (!function && !(oldOpConfig & BQ27421_OPCONFIG_BATLOWEN)))
        return true;

    // Modify BATLOWN_EN bit of opConfig:
    uint16_t newOpConfig = oldOpConfig;
    if (function)
        newOpConfig |= BQ27421_OPCONFIG_BATLOWEN;
    else
        newOpConfig &= ~(BQ27421_OPCONFIG_BATLOWEN);

    // Write new opConfig
    return writeOpConfig(newOpConfig);
}

// Get SOC1_Set Threshold - threshold to set the alert flag
uint8_t BQ27421::SOC1SetThreshold(void)
{
    return readExtendedData(BQ27421_ID_DISCHARGE, 0);
}

// Get SOC1_Clear Threshold - threshold to clear the alert flag
uint8_t BQ27421::SOC1ClearThreshold(void)
{
    return readExtendedData(BQ27421_ID_DISCHARGE, 1);
}

// Set the SOC1 set and clear thresholds to a percentage
bool BQ27421::setSOC1Thresholds(uint8_t set, uint8_t clear)
{
    uint8_t thresholds[2];
    thresholds[0] = constrain(set, 0, 100);
    thresholds[1] = constrain(clear, 0, 100);
    return writeExtendedData(BQ27421_ID_DISCHARGE, 0, thresholds, 2);
}

// Get SOCF_Set Threshold - threshold to set the alert flag
uint8_t BQ27421::SOCFSetThreshold(void)
{
    return readExtendedData(BQ27421_ID_DISCHARGE, 2);
}

// Get SOCF_Clear Threshold - threshold to clear the alert flag
uint8_t BQ27421::SOCFClearThreshold(void)
{
    return readExtendedData(BQ27421_ID_DISCHARGE, 3);
}

// Set the SOCF set and clear thresholds to a percentage
bool BQ27421::setSOCFThresholds(uint8_t set, uint8_t clear)
{
    uint8_t thresholds[2];
    thresholds[0] = constrain(set, 0, 100);
    thresholds[1] = constrain(clear, 0, 100);
    return writeExtendedData(BQ27421_ID_DISCHARGE, 2, thresholds, 2);
}

// Check if the SOC1 flag is set
bool BQ27421::socFlag(void)
{
    uint16_t flagState = flags();

    return flagState & BQ27421_FLAG_SOC1;
}

// Check if the SOCF flag is set
bool BQ27421::socfFlag(void)
{
    uint16_t flagState = flags();

    return flagState & BQ27421_FLAG_SOCF;

}

// Get the SOC_INT interval delta
uint8_t BQ27421::sociDelta(void)
{
    return readExtendedData(BQ27421_ID_STATE, 26);
}

// Set the SOC_INT interval delta to a value between 1 and 100
bool BQ27421::setSOCIDelta(uint8_t delta)
{
    uint8_t soci = constrain(delta, 0, 100);
    return writeExtendedData(BQ27421_ID_STATE, 26, &soci, 1);
}

// Pulse the GPOUT pin - must be in SOC_INT mode
bool BQ27421::pulseGPOUT(void)
{
    return executeControlWord(BQ27421_CONTROL_PULSE_SOC_INT);
}

/*****************************************************************************
 *************************** Control Sub-Commands ****************************
 *****************************************************************************/

// Read the device type - should be 0x0421
uint16_t BQ27421::deviceType(void)
{
    return readControlWord(BQ27421_CONTROL_DEVICE_TYPE);
}

// Enter configuration mode - set userControl if calling from an Arduino sketch
// and you want control over when to exitConfig
bool BQ27421::enterConfig(bool userControl)
{
    if (userControl) _userConfigControl = true;

    if (sealed())
    {
        _sealFlag = true;
        unseal(); // Must be unsealed before making changes
    }

    if (executeControlWord(BQ27421_CONTROL_SET_CFGUPDATE))
    {
        int16_t timeout = BQ72441_I2C_TIMEOUT;
        while ((timeout--) && (!(flags() & BQ27421_FLAG_CFGUPMODE)))
            delay(1);

        if (timeout > 0)
            return true;
    }

    return false;
}

// Exit configuration mode with the option to perform a resimulation
bool BQ27421::exitConfig(bool resim)
{
    // There are two methods for exiting config mode:
    //    1. Execute the EXIT_CFGUPDATE command
    //    2. Execute the SOFT_RESET command
    // EXIT_CFGUPDATE exits config mode _without_ an OCV (open-circuit voltage)
    // measurement, and without resimulating to update unfiltered-SoC and SoC.
    // If a new OCV measurement or resimulation is desired, SOFT_RESET or
    // EXIT_RESIM should be used to exit config mode.
    if (resim)
    {
        if (softReset())
        {
            int16_t timeout = BQ72441_I2C_TIMEOUT;
            while ((timeout--) && ((flags() & BQ27421_FLAG_CFGUPMODE)))
                delay(1);
            if (timeout > 0)
            {
                if (_sealFlag) seal(); // Seal back up if we IC was sealed coming in
                return true;
            }
        }
        return false;
    }
    else
    {
        return executeControlWord(BQ27421_CONTROL_EXIT_CFGUPDATE);
    }
}

// Read the flags() command
uint16_t BQ27421::flags(void)
{
    return readWord(BQ27421_COMMAND_FLAGS);
}

// Read the CONTROL_STATUS subcommand of control()
uint16_t BQ27421::status(void)
{
    return readControlWord(BQ27421_CONTROL_STATUS);
}

/***************************** Private Functions *****************************/

// Check if the BQ27421-G1A is sealed or not.
bool BQ27421::sealed(void)
{
    uint16_t stat = status();
    return stat & BQ27421_STATUS_SS;
}

// Seal the BQ27421-G1A
bool BQ27421::seal(void)
{
    return readControlWord(BQ27421_CONTROL_SEALED);
}

// UNseal the BQ27421-G1A
bool BQ27421::unseal(void)
{
    // To unseal the BQ27421, write the key to the control
    // command. Then immediately write the same key to control again.
    if (readControlWord(BQ27421_UNSEAL_KEY))
    {
        return readControlWord(BQ27421_UNSEAL_KEY);
    }
    return false;
}

// Read the 16-bit opConfig register from extended data
uint16_t BQ27421::opConfig(void)
{
    return readWord(BQ27421_EXTENDED_OPCONFIG);
}

// Write the 16-bit opConfig register in extended data
bool BQ27421::writeOpConfig(uint16_t value)
{
    uint8_t opConfigMSB = value >> 8;
    uint8_t opConfigLSB = value & 0x00FF;
    uint8_t opConfigData[2] = {opConfigMSB, opConfigLSB};

    // OpConfig register location: BQ27421_ID_REGISTERS id, offset 0
    return writeExtendedData(BQ27421_ID_REGISTERS, 0, opConfigData, 2);
}

// Issue a soft-reset to the BQ27421-G1A
bool BQ27421::softReset(void)
{
    return executeControlWord(BQ27421_CONTROL_SOFT_RESET);
}

// Read a 16-bit command word from the BQ27421-G1A
uint16_t BQ27421::readWord(uint16_t subAddress)
{
    uint8_t data[2];
    i2cReadBytes(subAddress, data, 2);
    return ((uint16_t) data[1] << 8) | data[0];
}

// Read a 16-bit subcommand() from the BQ27421-G1A's control()
uint16_t BQ27421::readControlWord(uint16_t function)
{
    uint8_t subCommandMSB = (function >> 8);
    uint8_t subCommandLSB = (function & 0x00FF);
    uint8_t command[2] = {subCommandLSB, subCommandMSB};
    uint8_t data[2] = {0, 0};

    i2cWriteBytes((uint8_t) 0, command, 2);

    if (i2cReadBytes((uint8_t) 0, data, 2))
    {
        return ((uint16_t)data[1] << 8) | data[0];
    }

    return false;
}

// Execute a subcommand() from the BQ27421-G1A's control()
bool BQ27421::executeControlWord(uint16_t function)
{
    uint8_t subCommandMSB = (function >> 8);
    uint8_t subCommandLSB = (function & 0x00FF);
    uint8_t command[2] = {subCommandLSB, subCommandMSB};

    if (i2cWriteBytes((uint8_t) 0, command, 2))
        return true;

    return false;
}

/*****************************************************************************
 ************************** Extended Data Commands ***************************
 *****************************************************************************/

// Issue a BlockDataControl() command to enable BlockData access
bool BQ27421::blockDataControl(void)
{
    uint8_t enableByte = 0x00;
    return i2cWriteBytes(BQ27421_EXTENDED_CONTROL, &enableByte, 1);
}

// Issue a DataClass() command to set the data class to be accessed
bool BQ27421::blockDataClass(uint8_t id)
{
    return i2cWriteBytes(BQ27421_EXTENDED_DATACLASS, &id, 1);
}

// Issue a DataBlock() command to set the data block to be accessed
bool BQ27421::blockDataOffset(uint8_t offset)
{
    return i2cWriteBytes(BQ27421_EXTENDED_DATABLOCK, &offset, 1);
}

// Read the current checksum using BlockDataCheckSum()
uint8_t BQ27421::blockDataChecksum(void)
{
    uint8_t csum;
    i2cReadBytes(BQ27421_EXTENDED_CHECKSUM, &csum, 1);
    return csum;
}

// Use BlockData() to read a byte from the loaded extended data
uint8_t BQ27421::readBlockData(uint8_t offset)
{
    uint8_t ret;
    uint8_t address = offset + BQ27421_EXTENDED_BLOCKDATA;
    i2cReadBytes(address, &ret, 1);
    return ret;
}

// Use BlockData() to write a byte to an offset of the loaded data
bool BQ27421::writeBlockData(uint8_t offset, uint8_t data)
{
    uint8_t address = offset + BQ27421_EXTENDED_BLOCKDATA;
    return i2cWriteBytes(address, &data, 1);
}

// Read all 32 bytes of the loaded extended data and compute a
// checksum based on the values.
uint8_t BQ27421::computeBlockChecksum(void)
{
    uint8_t data[32];
    i2cReadBytes(BQ27421_EXTENDED_BLOCKDATA, data, 32);

    uint8_t csum = 0;
    for (int i = 0; i < 32; i++)
    {
        csum += data[i];
    }
    csum = 255 - csum;

    return csum;
}

// Use the BlockDataCheckSum() command to write a checksum value
bool BQ27421::writeBlockChecksum(uint8_t csum)
{
    return i2cWriteBytes(BQ27421_EXTENDED_CHECKSUM, &csum, 1);
}

// Read a byte from extended data specifying a class ID and position offset
uint8_t BQ27421::readExtendedData(uint8_t classID, uint8_t offset)
{
    uint8_t retData = 0;
    if (!_userConfigControl) enterConfig(false);

    if (!blockDataControl()) // // enable block data memory control
        return false; // Return false if enable fails
    if (!blockDataClass(classID)) // Write class ID using DataBlockClass()
        return false;

    blockDataOffset(offset / 32); // Write 32-bit block offset (usually 0)

    computeBlockChecksum(); // Compute checksum going in
    blockDataChecksum();
    /*for (int i=0; i<32; i++)
        Serial.print(String(readBlockData(i)) + " ");*/
    retData = readBlockData(offset % 32); // Read from offset (limit to 0-31)

    if (!_userConfigControl) exitConfig();

    return retData;
}

// Write a specified number of bytes to extended data specifying a
// class ID, position offset.
bool BQ27421::writeExtendedData(uint8_t classID, uint8_t offset, uint8_t * data, uint8_t len)
{
    if (len > 32)
        return false;

    if (!_userConfigControl) enterConfig(false);

    if (!blockDataControl()) // // enable block data memory control
        return false; // Return false if enable fails
    if (!blockDataClass(classID)) // Write class ID using DataBlockClass()
        return false;

    blockDataOffset(offset / 32); // Write 32-bit block offset (usually 0)
    computeBlockChecksum(); // Compute checksum going in
    blockDataChecksum();

    // Write data bytes:
    for (int i = 0; i < len; i++)
    {
        // Write to offset, mod 32 if offset is greater than 32
        // The blockDataOffset above sets the 32-bit block
        writeBlockData((offset % 32) + i, data[i]);
    }

    // Write new checksum using BlockDataChecksum (0x60)
    uint8_t newCsum = computeBlockChecksum(); // Compute the new checksum
    writeBlockChecksum(newCsum);

    if (!_userConfigControl) exitConfig();

    return true;
}

/*****************************************************************************
 ************************ I2C Read and Write Routines ************************
 *****************************************************************************/
int BQ27421::i2c_init(const char * devPath)
{
    if ((_device_fd = open(devPath, O_RDWR)) < 0) {
        /* ERROR HANDLING: you can check errno to see what went wrong */
        perror("Failed to open the i2c bus");
        return -1;
    }
    if (ioctl(_device_fd, I2C_SLAVE, _deviceAddress) < 0) {
        printf("Failed to acquire bus access and/or talk to slave.\n");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        return -1;
    }
    return _device_fd;
}

// Read a specified number of bytes over I2C at a given subAddress
int16_t BQ27421::i2cReadBytes(uint8_t cmd, uint8_t * data, uint8_t len)
{
    if (write(_device_fd, &cmd, 1) != 1) {
        printf("Failed to write to the i2c bus.\n");
        return false;
    }

    if (read(_device_fd, data, len) != len) {
        printf("Failed to read from the i2c bus.\n");
        return false;
    }

    return true;
}

// Write a specified number of bytes over I2C to a given subAddress
uint16_t BQ27421::i2cWriteBytes(uint8_t cmd, uint8_t * data, uint8_t len)
{
    uint8_t * buf = (uint8_t*) malloc(len + 1);
    buf[0] = cmd;
    memcpy(buf + 1, data, len);
    if (write(_device_fd, buf, len + 1) != len + 1) {
        printf("Failed to write to the i2c bus.\n");
        free(buf);
        return false;
    }
    free(buf);
    return true;
}